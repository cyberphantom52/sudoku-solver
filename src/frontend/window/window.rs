/* window.rs
 *
 * Copyright 2022 Cyber Phantom
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate, Button, Stack, ListBox};
use adw::{Bin, Leaflet, ToastOverlay};
use glib::subclass::InitializingObject;

use once_cell::sync::OnceCell;

#[derive(Debug, Default, CompositeTemplate)]
#[template(resource = "/io/gitlab/cyberphantom52/sudoku_solver/ui/window.ui")]
pub struct SudokuSolverWindow {
    #[template_child]
    pub btn_new: TemplateChild<Button>,
    #[template_child]
    pub btn_back: TemplateChild<Button>,
    #[template_child]
    pub btn_reset: TemplateChild<Button>,
    #[template_child]
    pub btn_solve: TemplateChild<Button>,
    #[template_child]
    pub grid: TemplateChild<Bin>,
    #[template_child]
    pub leaflet: TemplateChild<Leaflet>,
    #[template_child]
    pub sudoku_list: TemplateChild<ListBox>,
    #[template_child]
    pub sudoku_solver_stack: TemplateChild<Stack>,
    #[template_child]
    pub toasts: TemplateChild<ToastOverlay>,

    pub sudokus: OnceCell<gio::ListStore>,
}

impl ObjectImpl for SudokuSolverWindow {
    fn constructed(&self) {
        self.parent_constructed();
        
        let obj = self.obj();
        obj.setup_sudokus();
        obj.setup_actions();
        obj.setup_callbacks();
        obj.load_sudokus();
    }
}

#[glib::object_subclass]
impl ObjectSubclass for SudokuSolverWindow {
    const NAME: &'static str = "SudokuSolverWindow";
    type Type = super::SudokuSolverWindow;
    type ParentType = adw::ApplicationWindow;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl WidgetImpl for SudokuSolverWindow {}

impl WindowImpl for SudokuSolverWindow {
    fn close_request(&self) -> gtk::Inhibit {
        self.obj().save_sudokus();
        self.parent_close_request()
    }
}

impl ApplicationWindowImpl for SudokuSolverWindow {}
impl AdwApplicationWindowImpl for SudokuSolverWindow {}

