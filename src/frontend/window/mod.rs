mod window;

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{glib, gio, ResponseType};

use super::sudoku_row::SudokuRow;
use super::sudoku_grid::SudokuGrid;

use super::sudoku_row::SudokuRowData;

use crate::SudokuSolverApplication;
use crate::utils::is_entry_valid;

use glib::{Object, clone, user_config_dir};

use std::fs::File;


glib::wrapper! {
    pub struct SudokuSolverWindow(ObjectSubclass<window::SudokuSolverWindow>)
        @extends adw::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget,
                    gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl SudokuSolverWindow {
    pub fn new(app: &SudokuSolverApplication) -> Self {
        Object::builder()
            .property("application", app)
            .build()
    }
    
    fn change_page(&self, direction: adw::NavigationDirection) {
        self.imp().leaflet.navigate(direction);
        self.imp().btn_back.set_visible(!self.imp().btn_back.is_visible());
        self.imp().btn_new.set_visible(!self.imp().btn_new.is_visible());
    }

    fn new_sudoku(&self) {
        // Calling this as a hack here to register SudokuGrid with GObject
        // Without this the template below fails to build
        SudokuGrid::static_type();

        // Setup Dialog
        let builder = gtk::Builder::from_resource("/io/gitlab/cyberphantom52/sudoku_solver/ui/dialog.ui");
        
        let dialog: gtk::Dialog = builder.object("dialog_new")
            .expect("There needs to be an object named `about`");

        dialog.set_transient_for(Some(self));
        
        let dialog_button = dialog
            .widget_for_response(ResponseType::Accept)
            .expect("The dialog needs to have a widget for response type `Accept`.");
        dialog_button.set_sensitive(false);


        let entry: adw::EntryRow = builder.object("entry")
            .expect("There needs to be an object named `entry`");


        let grid: SudokuGrid = builder.object("grid")
            .expect("There needs to be an object named `grid`");

        // Setup Dialog callbacks
        let is_dialog_valid = clone!(@weak dialog_button, @weak entry, @weak grid => move || {
            let num_error = grid.property::<u32>("num-error");
            
            if !is_entry_valid(&entry) {
                entry.add_css_class("error");
            } else {
                entry.remove_css_class("error");
            }
            
            if !is_entry_valid(&entry) || num_error != 0 {
                dialog_button.set_sensitive(false);
            } else {
                dialog_button.set_sensitive(true);
            }
        });

        entry.connect_changed(clone!(@strong is_dialog_valid => move |_| {
            is_dialog_valid();
        }));

        grid.connect_closure(
            "num-error-changed",
            true,
            glib::closure_local!(
                move |_: &SudokuGrid, _: u32| {
                    is_dialog_valid();
                }
            ),
        );

        dialog.connect_response(
            clone!(@weak self as window, @weak entry, @weak grid => move |dialog, response| {
                if response != ResponseType::Accept {
                    dialog.destroy();
                    return;
                }
                
                let title = entry.text().to_string();

                if let None = window.get_sudoku_row_index(title.as_str()) {
                    let sudoku_row = SudokuRow::new(&title);
                    grid.set_fixed();
                    sudoku_row.set_grid(grid);
                    window.sudokus().append(&sudoku_row);
                    window.show_toast(&format!("\"{title}\" has been added."), 3);
                    dialog.destroy();
                } else {
                    entry.add_css_class("error");
                }
            }),
        );
        dialog.present();
    }

    fn get_sudoku_row_index(&self, title: &str) -> Option<u32> {
        let sudokus = self.sudokus();
        let mut position = 0;
        while let Some(row) = sudokus.item(position) {
            if row.property::<String>("title") == title {
                return Some(position);
            }
            position += 1;
        }
        return None;
    }

    fn setup_actions(&self) {
        let action_new_sudoku = gio::SimpleAction::new("new-sudoku", None);
        action_new_sudoku.connect_activate(clone!(@weak self as window => move |_, _| {
            window.new_sudoku();
        }));
        self.add_action(&action_new_sudoku);

        let action_delete_sudoku = gio::SimpleAction::new("delete-sudoku", Some(&String::static_variant_type()));
        action_delete_sudoku.connect_activate(
            clone!(@weak self as window => move |_, value| {
                let name: String = value.unwrap()
                .get()
                .unwrap();
                
                let confirmation_dialog = adw::MessageDialog::builder()
                    .heading(&format!("Are you sure you want to delete “{name}”?"))
                    .body("This will permanently delete the Sudoku and its data.")
                    .close_response("cancel")
                    .default_response("cancel")
                    .transient_for(&window)
                    .build();
                confirmation_dialog.add_responses(&[("cancel", "_Cancel"), ("delete", "_Delete")]);
                confirmation_dialog.set_response_appearance("delete", adw::ResponseAppearance::Destructive);
                confirmation_dialog.connect_response(Some("delete"), move |_, _| {
                    if let Some(pos) = window.get_sudoku_row_index(&name) {
                        window.sudokus().remove(pos);
                        window.show_toast(&format!("“{name}” has been deleted."), 3);
                    }
                });
                confirmation_dialog.present();
            }),
        );
        self.add_action(&action_delete_sudoku);

        let action_solve_sudoku = gio::SimpleAction::new("solve-sudoku", None);
        action_solve_sudoku.connect_activate(clone!(@weak self as window => move |_, _| {
            let solved  = window.imp().grid
                        .get()
                        .child()
                        .expect("The grid needs to have a child.")
                        .downcast_ref::<SudokuGrid>()
                        .expect("The child of the grid needs to be a SudokuGrid.")
                        .sudoku()
                        .solve();
            if !solved {
                window.show_toast("Sudoku is not solvable", 3);
            }
            window.refresh_grid();
        }));
        self.add_action(&action_solve_sudoku);
        let action_show_about = gio::SimpleAction::new("show-about", None);
        action_show_about.connect_activate(clone!(@weak self as window => move |_, _| {
            let builder = gtk::Builder::from_resource("/io/gitlab/cyberphantom52/sudoku_solver/ui/about.ui");
            let about_window: adw::AboutWindow = builder.object("about")
                .expect("There needs to be an object named `about`");
            about_window.set_transient_for(Some(&window));
            about_window.present();
        }));
        self.add_action(&action_show_about);
    }

    fn setup_callbacks(&self) {
        self.imp().btn_back.connect_clicked(
            clone!(@weak self as window => move |_| {
                window.change_page(adw::NavigationDirection::Back);
            }),
        );

        self.imp().btn_reset.connect_clicked(clone!(@weak self as window => move |_| {
            window.imp().grid
                        .get()
                        .child()
                        .expect("The grid needs to have a child.")
                        .downcast_ref::<SudokuGrid>()
                        .expect("The child of the grid needs to be a SudokuGrid.")
                        .sudoku()
                        .reset();
            window.refresh_grid();
        }));

        self.sudokus().connect_items_changed(
            clone!(@weak self as window => move |_,_,_,_| {
                window.set_stack();
            }),
        );

        self.imp().sudoku_list.connect_row_activated(
            clone!(@weak self as window => move |_, sudoku_row| {
                let sudoku_row = sudoku_row
                    .downcast_ref::<SudokuRow>()
                    .expect("The object should be of type `SudokuRow`");
                let sudoku_grid = sudoku_row.imp()
                    .sudoku_grid
                    .get()
                    .expect("Value should be a `SudokuGrid`");
                window.imp().grid.set_child(Some(sudoku_grid));
                window.refresh_grid();
                window.change_page(adw::NavigationDirection::Forward);
            }),
        );
    }

    fn setup_sudokus(&self) {
        let sudokus = gio::ListStore::new(SudokuRow::static_type());
        self.imp()
            .sudokus
            .set(sudokus.clone())
            .expect("Could not set sudokus");
        
        self.imp().sudoku_list.bind_model(
            Some(&sudokus),
            clone!(@weak self as window => @default-panic, move |obj| {
                let row = obj
                    .downcast_ref::<SudokuRow>()
                    .expect("The object should be of type `SudokuRow`.")
                    .clone();
                row.upcast()
            }),
        )
    }

    fn save_sudokus(&self) {
        let backup_data: Vec<SudokuRowData> = self.sudokus()
            .snapshot()
            .iter()
            .filter_map(Cast::downcast_ref::<SudokuRow>)
            .map(SudokuRow::to_row_data)
            .collect();

        let file_path = user_config_dir().join("sudokus.json");
        let file = File::create(file_path).expect("Could not create file");
        serde_json::to_writer(file, &backup_data).expect("Could not write to file");
    }

    fn load_sudokus(&self) {
        let file_path = user_config_dir().join("sudokus.json");
        if let Ok(file) = File::open(file_path) {
            let backup_data: Vec<SudokuRowData> = serde_json::from_reader(file)
                .expect("Could not read file");

            let sudokus: Vec<SudokuRow> = backup_data
                .into_iter()
                .map(SudokuRow::from_row_data)
                .collect();

            self.sudokus().extend_from_slice(&sudokus);
        }
    }

    fn set_stack(&self) {
        if self.sudokus().n_items() > 0 {
            self.imp()
            .sudoku_solver_stack
            .set_visible_child_name("main");
            self.imp().btn_new.set_visible(true);
        } else {
            self.imp()
            .sudoku_solver_stack
            .set_visible_child_name("placeholder");
            self.imp().btn_new.set_visible(false);
        }
    }

    fn sudokus(&self) -> gio::ListStore {
        self.imp()
            .sudokus
            .get()
            .expect("`sudokus` should be set in `setup_sudokus`")
            .clone()
    }
    
    fn show_toast(&self, message: &str, timeout: u32) {
        let toast = adw::Toast::builder()
            .title(message)
            .timeout(timeout)
            .build();

        self.imp().toasts.add_toast(&toast);
    }

    fn refresh_grid(&self) {
        self.imp()
            .grid
            .get()
            .child()
            .expect("The grid needs to have a child.")
            .downcast_ref::<SudokuGrid>()
            .expect("The child of the grid needs to be a SudokuGrid.")
            .refresh();
    }
}

