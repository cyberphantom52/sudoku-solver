use std::cell::Cell;

use super::wfc::wave_function_collapse;

use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct SudokuCell {
    val: Cell<u8>,
    is_fixed: Cell<bool>,
    entropy: Cell<[bool; 9]>,
}

impl SudokuCell {
    pub fn new() -> Self {
        Self {
            val: Cell::new(0),
            is_fixed: Cell::new(false),
            entropy: Cell::new([true; 9]),
        }
    }

    pub fn from_cell(&self, cell: &SudokuCell) {
        self.set_val(cell.get_val());
        self.is_fixed.set(cell.is_fixed());
    }

    pub fn get_val(&self) -> u8 {
        self.val.get()
    }

    pub fn set_val(&self, val: u8) {
        self.val.replace(val);
        self.entropy.replace([false; 9]);
    }

    pub fn is_fixed(&self) -> bool {
        self.is_fixed.get()
    }

    pub fn set_fixed(&self) {
        if self.is_fixed.get() {
            return;
        }
        self.is_fixed.replace(true);
    }

    pub fn get_entropy(&self) -> usize {
        self.entropy.get()
            .iter()
            .filter(|&&x| x)
            .count()
    }

    pub fn possible_values(&self) -> Vec<u8> {
        let result: Vec<u8> = self.entropy.get()
            .iter()
            .enumerate()
            .filter(|(_, &x)| x)
            .map(|(i, _)| i as u8 + 1)
            .collect();

        result
    }

    pub fn remove_entropy(&self, value: u8) {
        if value == 0 {return}
        
        let mut entropy = self.entropy.get();
        entropy[(value - 1) as usize] = false;
        self.entropy.replace(entropy);
    }
}

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct Sudoku {
    board: [[SudokuCell; 9]; 9],
}

impl Sudoku {

    pub fn new() -> Self {
        let mut board : [[SudokuCell; 9]; 9] = Default::default();
        for row in 0..9 {
            for column in 0..9 {
                board[row][column] = SudokuCell::new();
            }
        }
        Self { board }
    }

    pub fn set_sudoku(&self, sudoku: Sudoku) {
        for row in 0..9 {
            for column in 0..9 {
                let cell = self.get(row, column);
                cell.from_cell(sudoku.get(row, column));
            }
        }
    }

    pub fn get(&self, row: usize, column: usize) -> &SudokuCell {
        &self.board[row][column]
    }

    fn is_valid_row(&self, row: usize, column: usize, value: u8) -> bool {
        for i in 0..9 {
            if i == column {continue};

            if self.board[row][i].get_val() == value {
                return false;
            }
        }
        return true;
    }

    fn is_valid_col(&self, row: usize, column: usize, value: u8) -> bool {
        for i in 0..9 {
            if i == row {continue};
            
            if self.board[i][column].get_val() == value {
                return false;
            }
        }
        return true;
    }

    fn is_valid_box(&self, row: usize, column: usize, value: u8) -> bool {
        let row_start = row - row % 3;
        let col_start = column - column % 3;
        for i in row_start..row_start + 3 {
            for j in col_start..col_start + 3 {
                if i == row && j == column {continue};

                if self.board[i][j].get_val() == value {
                    return false;
                }
            }
        }
        return true;
    }

    pub fn is_valid(&self, row: usize, column: usize) -> bool {
        let value = self.get(row, column).get_val();
        
        if value == 0 {return true};

        self.is_valid_row(row, column, value)   &&
        self.is_valid_col(row, column, value)   &&
        self.is_valid_box(row, column, value)
    }

    pub fn solve(&self) -> bool {
        const MAX_ATTEMPTS: usize = 100;
        self.set_entropy();
        for _ in 0..MAX_ATTEMPTS {
            let sudoku = self.clone();
            wave_function_collapse(&sudoku);
            if sudoku.is_solved() {
                self.set_sudoku(sudoku);
                return true;
            }
        }
        false
    }

    pub fn is_solved(&self) -> bool {
        for row in 0..9 {
            for column in 0..9 {
                if self.get(row, column).get_val() == 0 || 
                    !self.is_valid(row, column) 
                {
                    return false;
                }
            }
        }
        return true;
    }

    pub fn set_entropy(&self) {
        for row in 0..9 {
            for column in 0..9 {
                let cell = self.get(row, column);
                if !cell.is_fixed() {
                    self.board[row][column].entropy.replace([true; 9]);
                }
            }
        }
    }

    pub fn reset(&self) {
        for row in 0..9 {
            for column in 0..9 {
                let cell = self.get(row, column);
                if !cell.is_fixed() {
                    self.board[row][column].val.replace(0);
                }
            }
        }
    }
}
