use adw::prelude::*;
use adw::EntryRow;

pub fn is_entry_valid(entry: &EntryRow) -> bool {
    let text = entry.text();
    
    if text.is_empty() || text.chars().any(|c| c.is_ascii_punctuation()) {
        return false;
    }

    true
}